// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectResonanceGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTRESONANCE_API AProjectResonanceGameMode : public AGameModeBase
{
	GENERATED_BODY()

		virtual void StartPlay() override;
	
	
};
