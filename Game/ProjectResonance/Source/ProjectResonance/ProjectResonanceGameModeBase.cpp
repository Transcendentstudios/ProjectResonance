// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectResonanceGameModeBase.h"
#include "Engine.h"


AProjectResonanceGameModeBase::AProjectResonanceGameModeBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void AProjectResonanceGameModeBase::StartPlay()
{
	Super::StartPlay();

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("HELLO WORLD"));
	}
}
