// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "ProjectResonanceGameState.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeProjectResonanceGameState() {}
// Cross Module References
	PROJECTRESONANCE_API UClass* Z_Construct_UClass_AProjectResonanceGameState_NoRegister();
	PROJECTRESONANCE_API UClass* Z_Construct_UClass_AProjectResonanceGameState();
	ENGINE_API UClass* Z_Construct_UClass_AGameStateBase();
	UPackage* Z_Construct_UPackage__Script_ProjectResonance();
// End Cross Module References
	void AProjectResonanceGameState::StaticRegisterNativesAProjectResonanceGameState()
	{
	}
	UClass* Z_Construct_UClass_AProjectResonanceGameState_NoRegister()
	{
		return AProjectResonanceGameState::StaticClass();
	}
	UClass* Z_Construct_UClass_AProjectResonanceGameState()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameStateBase,
				(UObject* (*)())Z_Construct_UPackage__Script_ProjectResonance,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "ProjectResonanceGameState.h" },
				{ "ModuleRelativePath", "ProjectResonanceGameState.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AProjectResonanceGameState>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AProjectResonanceGameState::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900280u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AProjectResonanceGameState, 4182389213);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AProjectResonanceGameState(Z_Construct_UClass_AProjectResonanceGameState, &AProjectResonanceGameState::StaticClass, TEXT("/Script/ProjectResonance"), TEXT("AProjectResonanceGameState"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AProjectResonanceGameState);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
