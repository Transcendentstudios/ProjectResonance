// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTRESONANCE_ProjectResonanceGameMode_generated_h
#error "ProjectResonanceGameMode.generated.h already included, missing '#pragma once' in ProjectResonanceGameMode.h"
#endif
#define PROJECTRESONANCE_ProjectResonanceGameMode_generated_h

#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_RPC_WRAPPERS
#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectResonanceGameMode(); \
	friend PROJECTRESONANCE_API class UClass* Z_Construct_UClass_AProjectResonanceGameMode(); \
public: \
	DECLARE_CLASS(AProjectResonanceGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/ProjectResonance"), NO_API) \
	DECLARE_SERIALIZER(AProjectResonanceGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAProjectResonanceGameMode(); \
	friend PROJECTRESONANCE_API class UClass* Z_Construct_UClass_AProjectResonanceGameMode(); \
public: \
	DECLARE_CLASS(AProjectResonanceGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/ProjectResonance"), NO_API) \
	DECLARE_SERIALIZER(AProjectResonanceGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectResonanceGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectResonanceGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectResonanceGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectResonanceGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectResonanceGameMode(AProjectResonanceGameMode&&); \
	NO_API AProjectResonanceGameMode(const AProjectResonanceGameMode&); \
public:


#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectResonanceGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectResonanceGameMode(AProjectResonanceGameMode&&); \
	NO_API AProjectResonanceGameMode(const AProjectResonanceGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectResonanceGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectResonanceGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectResonanceGameMode)


#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_PRIVATE_PROPERTY_OFFSET
#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_12_PROLOG
#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_RPC_WRAPPERS \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_INCLASS \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_INCLASS_NO_PURE_DECLS \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectResonance_Source_ProjectResonance_ProjectResonanceGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
