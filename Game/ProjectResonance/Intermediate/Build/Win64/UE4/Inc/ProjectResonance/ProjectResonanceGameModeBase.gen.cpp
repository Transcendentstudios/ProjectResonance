// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "ProjectResonanceGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeProjectResonanceGameModeBase() {}
// Cross Module References
	PROJECTRESONANCE_API UClass* Z_Construct_UClass_AProjectResonanceGameModeBase_NoRegister();
	PROJECTRESONANCE_API UClass* Z_Construct_UClass_AProjectResonanceGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_ProjectResonance();
// End Cross Module References
	void AProjectResonanceGameModeBase::StaticRegisterNativesAProjectResonanceGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AProjectResonanceGameModeBase_NoRegister()
	{
		return AProjectResonanceGameModeBase::StaticClass();
	}
	UClass* Z_Construct_UClass_AProjectResonanceGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_ProjectResonance,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "ProjectResonanceGameModeBase.h" },
				{ "ModuleRelativePath", "ProjectResonanceGameModeBase.h" },
				{ "ObjectInitializerConstructorDeclared", "" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AProjectResonanceGameModeBase>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AProjectResonanceGameModeBase::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AProjectResonanceGameModeBase, 3076337616);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AProjectResonanceGameModeBase(Z_Construct_UClass_AProjectResonanceGameModeBase, &AProjectResonanceGameModeBase::StaticClass, TEXT("/Script/ProjectResonance"), TEXT("AProjectResonanceGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AProjectResonanceGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
