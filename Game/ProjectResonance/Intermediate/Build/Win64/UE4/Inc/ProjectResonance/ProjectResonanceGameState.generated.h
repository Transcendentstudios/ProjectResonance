// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTRESONANCE_ProjectResonanceGameState_generated_h
#error "ProjectResonanceGameState.generated.h already included, missing '#pragma once' in ProjectResonanceGameState.h"
#endif
#define PROJECTRESONANCE_ProjectResonanceGameState_generated_h

#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_RPC_WRAPPERS
#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectResonanceGameState(); \
	friend PROJECTRESONANCE_API class UClass* Z_Construct_UClass_AProjectResonanceGameState(); \
public: \
	DECLARE_CLASS(AProjectResonanceGameState, AGameStateBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/ProjectResonance"), NO_API) \
	DECLARE_SERIALIZER(AProjectResonanceGameState) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAProjectResonanceGameState(); \
	friend PROJECTRESONANCE_API class UClass* Z_Construct_UClass_AProjectResonanceGameState(); \
public: \
	DECLARE_CLASS(AProjectResonanceGameState, AGameStateBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/ProjectResonance"), NO_API) \
	DECLARE_SERIALIZER(AProjectResonanceGameState) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectResonanceGameState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectResonanceGameState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectResonanceGameState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectResonanceGameState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectResonanceGameState(AProjectResonanceGameState&&); \
	NO_API AProjectResonanceGameState(const AProjectResonanceGameState&); \
public:


#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectResonanceGameState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectResonanceGameState(AProjectResonanceGameState&&); \
	NO_API AProjectResonanceGameState(const AProjectResonanceGameState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectResonanceGameState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectResonanceGameState); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectResonanceGameState)


#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_PRIVATE_PROPERTY_OFFSET
#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_12_PROLOG
#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_PRIVATE_PROPERTY_OFFSET \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_RPC_WRAPPERS \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_INCLASS \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_PRIVATE_PROPERTY_OFFSET \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_INCLASS_NO_PURE_DECLS \
	ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectResonance_Source_ProjectResonance_ProjectResonanceGameState_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
